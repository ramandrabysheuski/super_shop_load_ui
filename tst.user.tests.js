const DevConfig = require('./dev.config');


exports.config = {

    specs: [

    ],

    suites: {


        main: [


            './test/specs/LogInTests/*.js'

        ]

    },

    exclude: [

    ],

    capabilities: [{

        maxInstances: 50,

        browserName: 'chrome'
    }],

    sync: true,

    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'verbose',

    coloredLogs: true,

    deprecationWarnings: true,

    bail: 0,

    // screenshotPath: './errorShots/',

    baseUrl: 'https://pangaia-tst.bam-boo.eu/',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 180000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 900000,
    //
    // Default request retries count
    connectionRetryCount: 3,

    framework: 'jasmine',

    reporters: ['dot'],

    // reporterOptions: {
    //     allure: {
    //         outputDir: './allure_reports_tst'
    //     }
    // },

    jasmineNodeOpts: {

        defaultTimeoutInterval: 180000,

        expectationResultHandler: function(passed, assertion) {
            // do something
        }
    },

    // onPrepare: function() {
    //     DevConfig.connection_tst.execute(`DELETE bm FROM smsline.base_msisdns bm JOIN smsline.bases b ON b.id = bm.base_id WHERE b.user_id = ${DevConfig.tst_db_user_id}`);
    //
    //     DevConfig.connection_tst.execute(`DELETE  psc
    //         FROM smsline.bases b
    //         JOIN planned_spams ps ON ps.base_id = b.id
    //         JOIN planned_spam_channels psc ON psc.planned_spam_id = ps.id
    //         WHERE b.user_id = ${DevConfig.tst_db_user_id}`);
    //
    //     DevConfig.connection_tst.execute(`DELETE  ps
    //         FROM smsline.bases b
    //         JOIN planned_spams ps ON ps.base_id = b.id
    //         WHERE b.user_id = ${DevConfig.tst_db_user_id}`);
    //
    //     DevConfig.connection_tst.execute(`DELETE  b
    //         FROM smsline.bases b
    //         WHERE b.user_id = ${DevConfig.tst_db_user_id}`);
    //
    //     DevConfig.connection_tst.execute(`DELETE FROM smsline.blacklist WHERE user_id = ${DevConfig.tst_db_user_id}`);
    //
    //
    //
    // },

//     beforeSession: function (config, capabilities, specs) {
//         require("babel-core/register")({
//     presets: ['es2015']
// });
//     },


    before: function () {
        browser.addCommand("del_db_u_bases", function () {
            {

                /// Block for regular rd user


                DevConfig.connection_tst.execute(`DELETE bm FROM smsline.base_msisdns bm JOIN smsline.bases b ON b.id = bm.base_id WHERE b.user_id = ${DevConfig.tst_db_user_id}`);

                DevConfig.connection_tst.execute(`DELETE  psc
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            JOIN planned_spam_channels psc ON psc.planned_spam_id = ps.id
            WHERE b.user_id = ${DevConfig.tst_db_user_id}`);

                DevConfig.connection_tst.execute(`DELETE  ps
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            WHERE b.user_id = ${DevConfig.tst_db_user_id}`);

                DevConfig.connection_tst.execute(`DELETE  b
            FROM smsline.bases b
            WHERE b.user_id = ${DevConfig.tst_db_user_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.blacklist WHERE user_id = ${DevConfig.tst_db_user_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.messages_log WHERE user_id = ${DevConfig.tst_db_user_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.message_templates WHERE user_id = ${DevConfig.tst_db_user_id}`);


                /// Block for rd-async user

                DevConfig.connection_tst.execute(`DELETE bm FROM smsline.base_msisdns bm JOIN smsline.bases b ON b.id = bm.base_id WHERE b.user_id = ${DevConfig.tst_db_user_async_id}`);

                DevConfig.connection_tst.execute(`DELETE  psc
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            JOIN planned_spam_channels psc ON psc.planned_spam_id = ps.id
            WHERE b.user_id = ${DevConfig.tst_db_user_async_id}`);

                DevConfig.connection_tst.execute(`DELETE  ps
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            WHERE b.user_id = ${DevConfig.tst_db_user_async_id}`);

                DevConfig.connection_tst.execute(`DELETE  b
            FROM smsline.bases b
            WHERE b.user_id = ${DevConfig.tst_db_user_async_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.blacklist WHERE user_id = ${DevConfig.tst_db_user_async_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.messages_log WHERE user_id = ${DevConfig.tst_db_user_async_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.message_templates WHERE user_id = ${DevConfig.tst_db_user_async_id}`);

                /// Block for rd-limited user

                DevConfig.connection_tst.execute(`DELETE bm FROM smsline.base_msisdns bm JOIN smsline.bases b ON b.id = bm.base_id WHERE b.user_id = ${DevConfig.tst_db_user_limited_id}`);

                DevConfig.connection_tst.execute(`DELETE  psc
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            JOIN planned_spam_channels psc ON psc.planned_spam_id = ps.id
            WHERE b.user_id = ${DevConfig.tst_db_user_limited_id}`);

                DevConfig.connection_tst.execute(`DELETE  ps
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            WHERE b.user_id = ${DevConfig.tst_db_user_limited_id}`);

                DevConfig.connection_tst.execute(`DELETE  b
            FROM smsline.bases b
            WHERE b.user_id = ${DevConfig.tst_db_user_limited_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.blacklist WHERE user_id = ${DevConfig.tst_db_user_limited_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.messages_log WHERE user_id = ${DevConfig.tst_db_user_limited_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.message_templates WHERE user_id = ${DevConfig.tst_db_user_limited_id}`);

                /// Block for rd-utf-off user

                DevConfig.connection_tst.execute(`DELETE bm FROM smsline.base_msisdns bm JOIN smsline.bases b ON b.id = bm.base_id WHERE b.user_id = ${DevConfig.tst_db_user_utf_off_id}`);

                DevConfig.connection_tst.execute(`DELETE  psc
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            JOIN planned_spam_channels psc ON psc.planned_spam_id = ps.id
            WHERE b.user_id = ${DevConfig.tst_db_user_utf_off_id}`);

                DevConfig.connection_tst.execute(`DELETE  ps
            FROM smsline.bases b
            JOIN planned_spams ps ON ps.base_id = b.id
            WHERE b.user_id = ${DevConfig.tst_db_user_utf_off_id}`);

                DevConfig.connection_tst.execute(`DELETE  b
            FROM smsline.bases b
            WHERE b.user_id = ${DevConfig.tst_db_user_utf_off_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.blacklist WHERE user_id = ${DevConfig.tst_db_user_utf_off_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.messages_log WHERE user_id = ${DevConfig.tst_db_user_utf_off_id}`);

                DevConfig.connection_tst.execute(`DELETE FROM smsline.message_templates WHERE user_id = ${DevConfig.tst_db_user_utf_off_id}`);

            }
        });
    },

    // beforeCommand: function (commandName, args) {
    // },


    // beforeSuite: function (suite) {
    // },

    beforeTest: function (test) { browser.windowHandleSize({width: 1600, height: 900});
    },

    // beforeHook: function () {
    // },

    // afterHook: function () {
    // },

    // afterTest: function () {
    // },

    // afterSuite: function (suite) {
    // },


    // afterCommand: function (commandName, args, result, error) {
    // },

    // after: function (result, capabilities, specs) {
    // },

    // afterSession: function (config, capabilities, specs) {
    // },

    // onComplete: function(exitCode, config, capabilities) {
    // }
};
