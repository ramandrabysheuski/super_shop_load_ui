const LogInPage = require('../../pageobjects/LogInPage');
const MainPage = require('../../pageobjects/MainPage');

let logInPage = new LogInPage();
let mainPage = new MainPage();

const test_name = "CartTest_1.";


describe(`${test_name}`, function() {
    it(`${test_name}`, function(done) {


        logInPage.login();

        mainPage.elem_click(mainPage.ButtonCatalog);

        mainPage.elem_click(mainPage.ButtonAddToCart);

        mainPage.elem_click(mainPage.ButtonCart);

        mainPage.elem_click(mainPage.ButtonRemove);

        browser.call(done);


    });
});
