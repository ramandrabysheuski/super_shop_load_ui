let Page = require('../pageobjects/page');

class LogInPage extends Page {


    /**
     *                      page buttons
     */


    get ButtonSubmit() {

        return ("//button[@type='submit']");
    }

    get ButtonLogin() {

        return ("//a[@routerlink='/login']");
    }



    /**
     *                      page fields
     */

    get FieldUserName() {

        return ("//input[@type='email']");
    }

    get FieldUserPassword() {

        return ("//input[@type='password']");
    }


    /**
     *                      page alerts
     */


    /**
     *                      page methods
     */


    open() {
        super.open.call(this, '/#!/login');

        this.wait_for_spinner_to_disappear();

    }

    submit() {

        browser.click(this.ButtonSubmit);
    }

    login() {

        this.open();

        this.elem_click(this.ButtonLogin);

        browser.waitForVisible(this.FieldUserName, 50000);

        browser.waitForEnabled(this.FieldUserPassword, 50000);

        this.elem_set_value(this.FieldUserName, 'test@mail.ru');

        this.elem_set_value(this.FieldUserPassword, 'password');

        this.submit();

        // browser.waitForVisible("//img[@alt='Angular Logo']", 50000);
        //
        // browser.waitForEnabled("//img[@alt='Angular Logo']", 50000)

    }

}

module.exports = LogInPage;
