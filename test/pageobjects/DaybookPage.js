const ProfilePage = require('../pageobjects/ProfilePage');

class DaybookPage extends ProfilePage {


    /**
     *                      page buttons
     */


    get ButtonNextWeek() {

        return ("//li[@ng-click='allFunctions.changeCurrWeek(1)']");
    }

    get ButtonPrevWeek() {

        return ("//li[@ng-click='allFunctions.changeCurrWeek(-1)']");
    }





}

module.exports = DaybookPage;
