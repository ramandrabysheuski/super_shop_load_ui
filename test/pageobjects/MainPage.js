let Page = require('../pageobjects/page');


class MainPage extends Page {


    /**
     *                      left pane buttons
     */



    get ButtonCatalog() {

        return ("//a[@routerlink='/catalog']");
    }

    get ButtonAddToCart() {

        return ("//button[contains(text(), 'Add to cart')]");
    }

    get ButtonCart() {

        return ("//a[@routerlink='/cart']");
    }

    get ButtonRemove() {

        return ("//button[contains(text(), 'Remove!')]");
    }










}

module.exports = MainPage;
