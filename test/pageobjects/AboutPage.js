let Page = require('../pageobjects/page');

// let page = new Page();
class AboutPage extends Page {


    /**
     *                      page buttons
     */


    get ButtonAppstore() {

        return ("//a[contains(@class, 'download-apple')]");
    }

    get ButtonGooglePlay() {

        return ("//a[contains(@class, 'download-android')]");
    }





}

module.exports = AboutPage;
