const ProfilePage = require('../pageobjects/ProfilePage');

class ContactPage extends ProfilePage {


    /**
     *                      page icons
     */

    get IconEmail() {

        return ("//img[@src='/images/email_icon.svg']");
    }

    get IconPhone() {

        return ("//img[@src='/images/phone_icon.svg']");
    }





}

module.exports = ContactPage;
