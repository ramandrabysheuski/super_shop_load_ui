

let TestData = Object.create({


    /**
     *                      Teachers's user name list
     */

    UserName_director: "992908001001", //директор школы AutoTest

    UserName_head_teacher_pt_ruler_11B: "992908001012", //завуч школы учитель физры для одной подгруппы и классный руководитель 11Б

    UserName_teacher_pt: "992908001010", //учитель физры в одной подгруппе

    UserName_head_teacher_hstr: "992908001002", //завуч и учитель истории

    UserName_few_topics_ruler_11A: "992908001008", //учитель нескольких предметов (art | modern studies) и классный руководитель 11А

    UserName_teacher_eng: "992908001003", //учитель английского ( 1-ая подгруппа )

    UserName_teacher_eng_2: "992908001004", //учитель английского ( 2-ая подгруппа )

    UserName_teacher_geo_parent: "992908001005", //учитель географии и родитель

    UserName_teacher_math_twice: "992908001009", //учитель математики дважды в день

    UserName_teacher_director: "992908001007", //учитель в одной школе и директор в другой

    UserName_teacher_2_factor: "992908001011", //учитель с включенной 2-факторкой

    /**
     *                      Parents's user name list: Premium tariff
     */

    UserName_parent_1: "9929080010131", // 2 ребенка, учатся в одном классе, 3-й в другом

    UserName_parent_2: "992908001014", // 2 ребенка и учатся в разных школах

    //у 2-го ребенка есть реклама ( top banner + left )

    UserName_parent_3: "992908001013", // 2 ребенка, учатся в разных классах

    UserName_parent_4: "992908001005", // родитель и учитель

    /**
     *                      Parents's user name list: Standard tariff
     */

    UserName_parent_standard: "992908001016",

    /**
     *                      Parents's user name list: Base tariff
     */

    UserName_parent_base: "992908001015",


    /**
     *                      Pupils's user name list: Premium tariff
     */

    UserName_kid_1: "9929080010131",

    UserName_kid_2: "9929080010132",

    UserName_kid_3: "9929080010001",

    UserName_kid_4: "9929080010002",

    UserName_kid_5: "9929080010003",

    /**
     *                      Kids's user name list: Standard tariff
     */

    UserName_kid_standard: "9929080010161",

    /**
     *                      Kids's user name list: Base tariff
     */

    UserName_kid_base: "9929080010151",


});



module.exports = TestData;
