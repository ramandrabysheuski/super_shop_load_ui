let Page = require('../pageobjects/page');

// let page = new Page();
class ProfilePage extends Page {


    /**
     *                      page buttons
     */


    get ButtonSend() {

        return ("//button[@ng-click='allFunctions.sendTicket()']");
    }

    /**
     *                      page fields
     */

    get FieldSubject() {

        return ("//input[@ng-model='subject']");
    }

    get FieldText() {

        return ("//textarea[@ng-model='text']");
    }


    /**
     *                      page alerts
     */


    get AlertMessageNotSent() {

        return ("//span[@ng-if=\"data.actionError == 'reviewSendError'\"]");
    }

    get AlertMessageIsSent() {

        return ("//div[@ng-if='data.actionSuccess']");
    }




}

module.exports = ProfilePage;
