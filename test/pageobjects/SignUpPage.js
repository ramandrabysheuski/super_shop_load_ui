var Page = require('../pageobjects/page');


class SignUpPage extends Page {


    /**
     *                  page buttons
     **/


    get ButtonCloseTutorial() {

        return ("//div[@class='-close ng-scope' and @ng-click='$dismiss()']");
    }

    get ButtonSignIn() {

        return ("//a[@href='#!/login']");
    }

    get ButtonThankYou() {

        return ("//button[@class='btn btn-blue modal-btn auto-register__btn ng-scope' and @ng-click='$dismiss()']");
    }

    get ButtonAnyRegistrationProblems() {

        return ("//a[@href='#']");
    }


    /**
     *                  page popups
     **/

    get PopUpTutorial() {

        return ("//div[@class='F1 ng-scope']");
    }

    /**
     *                  page methods
     **/

    open() {
        super.open.call(this, '/#!/login');

        this.wait_for_spinner_to_disappear();
    }


}

module.exports = SignUpPage;
