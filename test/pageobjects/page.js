

class Page {


    open(path) {

        browser.url(path)
    }

    elem_click(element) {

        browser.waitForVisible(element, 90000);

        browser.waitForEnabled(element, 90000);

        browser.click(element);

        this.wait_for_spinner_to_disappear();

    }

    elem_set_value(element, value) {

        browser.waitForVisible(element, 90000);

        browser.waitForEnabled(element, 90000);

        browser.setValue(element, value)
    }

    elem_with_text_is_visible(text) {

        return !! browser.isVisible(`//*[contains(text(), '${text}')]`, 10000);
    }

    elem_is_visible(element) {

        return !! browser.isVisible(element, 10000);
    }


    wait_for_spinner_to_disappear () {

        browser.pause(900);

        if (browser.isVisible('.spinner-container.showSpinner')) {

            browser.waitForVisible('.spinner-container.showSpinner', 40000, true);
        }

        else if (browser.isVisible("//div[@class='modal video-register fade ng-scope ng-isolate-scope in']")) {

            browser.waitForVisible("//div[@class='modal video-register fade ng-scope ng-isolate-scope in']", 10000, true);
        }

        else {

        }
    }
}



module.exports = Page;
