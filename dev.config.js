const mysql = require('mysql2');
let DevConfig;
DevConfig = Object.create({

    // ui tests user credentials:

    User_password: "eQ1313",


    //db connector configs:

    connection_tst : mysql.createConnection({
        host: '10.7.4.21',
        user: 'smsline_api',
        password: 'Sm$L1n3AP',
        database: 'smsline',
        port: '3306',
        secure_auth: 'false'
    }),

    connection_stg : mysql.createConnection({
        host: '10.6.5.15',
        user: 'autotests',
        password: 't8g3WF6',
        database: 'smsline',
        port: '3306',
        secure_auth: 'false'
    }),


    tst_db_user_id: '3319',

    tst_db_user_async_id: '3334',

    tst_db_user_limited_id: '3332',

    tst_db_user_utf_off_id: '3331',


    stg_db_user_id: '1156',

    stg_db_user_async_id: '1161',

    stg_db_user_limited_id: '1159',

    stg_db_user_utf_off_id: '1158',






});

module.exports = DevConfig;
